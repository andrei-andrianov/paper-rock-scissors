package controller;

import org.junit.jupiter.api.*;
import util.Utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GameControllerTest {

    private GameController gameController;

    @BeforeEach
    void setUp() {
        gameController = GameController.getInstance();
    }

    @Test
    @Order(1)
    @DisplayName("Test calculateScore() with player wins")
    void testCalculateScorePlayerWins() {
        gameController.calculateScore("rock", "scissors");
        assertEquals(1, gameController.scorePlayer);
        assertEquals(0, gameController.scoreComputer);
    }

    @Test
    @Order(2)
    @DisplayName("Test calculateScore() with computer wins")
    void testCalculateScoreComputerWins() {
        gameController.calculateScore("rock", "paper");
        assertEquals(1, gameController.scorePlayer);
        assertEquals(1, gameController.scoreComputer);
    }

    @Test
    @Order(3)
    @DisplayName("Test calculateScore() with tie")
    void testCalculateScoreTie() {
        gameController.calculateScore("rock", "rock");
        assertEquals(1, gameController.scorePlayer);
        assertEquals(1, gameController.scoreComputer);
    }

    @Test
    @DisplayName("Test validateInput() with valid input")
    void testValidateInputWithValidInput() {
        String input = "rock\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals("rock", gameController.validateInput(Utils.scanner.nextLine()));
    }

}
