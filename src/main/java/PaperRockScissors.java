import controller.GameController;
import util.Utils;

public class PaperRockScissors {
    private static final String NEW_GAME_MESSAGE = """
                ####################################################
                ### Welcome to the Paper-Rock-Scissors CLI game! ###
                ####################################################
                
                              Following are the rules:
                Player enters the amount of rounds for the current game.
                Round start when user presses enter.
                When the round starts, on count 3, 2, 1 both computer and player must choose their symbol.
                Player enters his input as either "rock", "scissors", "paper".
                Results of the current round are shown to the player.
                Next round starts after user presses enter.
                Final score is displayed after all the rounds are finished.
                
                ####################################################
                #################### Good luck ! ###################
                ####################################################
                """;


    public static void main(String[] args) {
        System.out.println(NEW_GAME_MESSAGE);
        System.out.println("\nPlease enter the amount of rounds you would like to play:");
        int n = Utils.scanner.nextInt();

        var newGame = GameController.getInstance();
        newGame.playGame(n);

        System.out.println("All rounds are finished! Thank you for playing.");
    }

}
