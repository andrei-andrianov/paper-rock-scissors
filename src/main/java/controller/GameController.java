package controller;

import util.Utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class GameController {

    private static final List<String> symbolsInPlay = List.of("rock", "scissors", "paper");
    private static GameController instance = null;

    int scoreComputer = 0;
    int scorePlayer = 0;

    private GameController() {
    }

    public static GameController getInstance() {
        if (instance == null) {
            instance = new GameController();
        }
        return instance;
    }

    public void playGame(int numberOfRounds) {
        var isGameOver = false;
        var currentRound = 1;
        var symbolPlayer = "";

        // Game Loop
        while (!isGameOver) {
            System.out.println("Round #" + currentRound +
                    " is about to start! Press enter when you're ready to begin.");
            var startRound = Utils.scanner.nextLine();

            startCounter();
            System.out.println("Please enter your input: ");
            symbolPlayer = Utils.scanner.nextLine();

            validateInput(symbolPlayer);

            var symbolComputer = symbolsInPlay.get(Utils.random.nextInt(symbolsInPlay.size()));

            calculateScore(symbolPlayer, symbolComputer);

            System.out.println("Round is over! This round computer picked: " + symbolComputer);
            System.out.println("Current score:\nComputer:\t" + scoreComputer + "\nPlayer:\t" + scorePlayer);
            System.out.println("To start new round press enter.");
            startRound = Utils.scanner.nextLine();

            if (currentRound == numberOfRounds){
                isGameOver = true;
                System.out.println("Final score:\nComputer:\t" + scoreComputer + "\nPlayer:\t" + scorePlayer);
            }

            currentRound++;
        }
    }

    void calculateScore(String symbolPlayer, String symbolComputer) {
        if (symbolPlayer.equals(symbolComputer)){
            return;
        }
        if (symbolPlayer.equals("rock")){
            if (symbolComputer.equals("scissors")){
                scorePlayer++;
            } else {
                scoreComputer++;
            }
        }
        if (symbolPlayer.equals("scissors")){
            if (symbolComputer.equals("paper")){
                scorePlayer++;
            } else {
                scoreComputer++;
            }
        }
        if (symbolPlayer.equals("paper")){
            if (symbolComputer.equals("rock")){
                scorePlayer++;
            } else {
                scoreComputer++;
            }
        }
    }

    void startCounter(){
        try {
            for (int i = 3; i > 0; i --){
                TimeUnit.SECONDS.sleep(1);
                System.out.println(i + "...");
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    String validateInput(String input){
        var isValidInput = false;

        while (!isValidInput){
            if (!symbolsInPlay.contains(input)){
                System.out.println("Sorry, I did not recognize that. Let's try again: ");
                input = Utils.scanner.nextLine();
                return validateInput(input);
            } else {
                isValidInput = true;
            }
        }

        return input;
    }

}
