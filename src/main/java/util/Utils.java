package util;

import java.util.Random;
import java.util.Scanner;

public class Utils {

    public static final Scanner scanner = new Scanner(System.in);
    public static final Random random = new Random();

    private Utils(){
        throw new IllegalStateException("Utility class");
    }

}
